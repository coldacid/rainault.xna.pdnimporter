using System.IO;
using Microsoft.Xna.Framework.Content.Pipeline;
using PaintDotNet;

namespace Rainault.Xna.PdnImporter
{
   [ContentImporter(".pdn", DisplayName = "Paint.NET Importer", DefaultProcessor = "PdnProcessor")]
   public class PdnImporter : ContentImporter<Document>
   {
      public override Document Import(string filename, ContentImporterContext context)
      {
         return Document.FromStream(File.OpenRead(filename));
      }
   }
}
