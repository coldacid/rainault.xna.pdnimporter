using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics.PackedVector;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using PaintDotNet;

using Color = System.Drawing.Color;
using Rgba32 = Microsoft.Xna.Framework.Color;

namespace Rainault.Xna.PdnImporter
{
    [ContentProcessor(DisplayName = "Paint.NET Processor")]
    public class PdnProcessor : ContentProcessor<Document, TextureContent>
    {
        public override TextureContent Process(Document input, ContentProcessorContext context)
        {
            Surface surface = new Surface(input.Size);
            input.Flatten(surface);
            Bitmap bmp = surface.CreateAliasedBitmap();

            PixelBitmapContent<Rgba32> bmpContent = new PixelBitmapContent<Rgba32>(bmp.Width, bmp.Height);
            for (int y = 0; y < bmp.Height; ++y)
            {
                for (int x = 0; x < bmp.Width; ++x)
                {
                    Color pixel = bmp.GetPixel(x, y);
                    bmpContent.SetPixel(x, y, new Rgba32((float)pixel.R / 255,
                                                         (float)pixel.G / 255,
                                                         (float)pixel.B / 255,
                                                         (float)pixel.A / 255));
                }
            }

            Texture2DContent texture = new Texture2DContent();
            texture.Mipmaps = new MipmapChain(bmpContent);
            texture.Validate(context.TargetProfile);

            TextureProcessor proc = new TextureProcessor();
            return proc.Process(texture, context);
        }
    }
}