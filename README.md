Rainault.Xna.PdnImporter
========================

XNA Paint.NET Importer and Content Processor.

Originally written for XNA 3.1 by Daniel F. "Rainault" Hanson, updated to XNA
4.0 by Chris "coldacid" Charabaruk.

Original site: https://pdnimporter.codeplex.com/

This content processor allows you to use Paint.NET .pdn images as textures or
sprite sheets in your XNA 4.0 project. Simply add a reference to
Rainault.Xna.PdnImporter to your content project, and you will be able to use
.pdn files -- no more exporting to PNG or JPG first!

How to Use PdnImporter
----------------------

PdnImporter requires that Paint.NET is installed on your computer before it
can be used to process .pdn files. You will need to copy certain files from
your Paint.NET install directory to the folder containing Rainault.Xna.
PdnImporter.dll. (If you are building PdnImporter yourself, Visual Studio
should handle copying these files for you.)

Copy these files from the Paint.NET directory to PdnImporter:

 * ICSharpCode.SharpZipLib.dll
 * Interop.WIA.dll
 * PaintDotNet.Base.dll
 * PaintDotNet.Core.dll
 * PaintDotNet.Data.dll
 * PaintDotNet.Resources.dll
 * PaintDotNet.SystemLayer.dll
 * PaintDotNet.SystemLayer.Native.x86.dll

In addition, you need to copy the Native.x86/ folder from Paint.NET to the
PdnImporter folder. Unfortunately Visual Studio does not copy this one, so if
you are building PdnImporter yourself, remember to copy it!

After you have PdnImporter all set up, add it as a reference to your content
project. You should be able to add .pdn files to your project now, as you
would other image files. The Visual Studio properties window should show
"Paint.NET Importer" for the content importer, and "Paint.NET Processor" or
"Paint.NET Sprite Sheet Processor" for the content processor. The resulting
content for either processor is a Texture2D object that you can load normally
in your game project.

**NOTICE:** If you get the message "Error loading pipeline assembly
"PaintDotNet.SystemLayer.Native.x64.dll" you need to delete that file from the
folder where Rainault.Xna.PdnImporter.dll resides. This file may be copied by
Visual Studio if you are running a 64-bit edition of Windows. After deleting
the file, the importer should work properly.

PdnImporter Processors
----------------------

PdnImporter comes with two different content processors, which treat layers in
a .pdn file differently:

 * The basic **Paint.NET Processor** flattens image layers together when
   making a texture.
 * The **Paint.NET Sprite Sheet Processor** organizes layers into a grid of
   rows and columns. You can choose the number of layers per row with the
   SpritesPerRow property of the content processor.

Both processors create a Texture2D object that can be used as normal in your
game project.

The MIT License (MIT)
---------------------

Copyright (c) 2009 Daniel F. Hanson, 2013 Christopher S. Charabaruk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
